## http请求框架

v1.0 => 单请求服务

[v1.1](v1.1.md) => 可同时注册多请求服务，满足https,http 同时构建 


###  http请求框架
1. 自定多路由注册解析
2. html模板解析

获取
```
go get -u  gitee.com/tym_hmm/eric-http
```

使用
```
/**
静态打包处理需开启
**/
//go:embed Asset
var publicAsset embed.FS

func TestHttp(t *testing.T)  {

 server :=  EricServer.NewEricServer(&publicAsset)
 server.
    //设置模板
    SetTemplate("Asset/Template", ".html").
    //设置json响应
    SetResponseKey("code", "message", "data").
    //设置静态资源加载 js css ,会打包到二进制包中
    SetStaticDir("/static/", "/Asset/Static").
    //多个静态资源加载设置
    //SetStaticDir("/static2/", "/Asset/Static2").
    //设置资源上传及访问目录
    SetUploadDIr("/upload", "/Store").
    //设置路由
    RegisterHttpRoute(ApiRouteNew, HttpRouteNew).
    Start("0.0.0.0", 8081, 1, EricServer.MODE_CONST_DEV)
}


/**
http 路由注册
*/
func HttpRouteNew(e *Route.Engine) {
    e.Get("/", RequestMiddleware(), func(c *Route.Context) {
      //多模板解析
      //c.TemplateMultiple()
      //单模板解析
      //c.Template()
      _, _ = c.Writer.Write([]byte("这是是ttp路由"))
    })
}

/**
api 路由注册
*/
func ApiRouteNew(e *Route.Engine) {

    v1:=e.Group("/v1")
    v1.UseMiddleware(RequestMiddleware())
    {
      v1.Get("/api", func(c *Route.Context) {
        c.JsonResponse(100, "成功", nil)
      })
    }
}

/**
请求日志拦截
*/
func RequestMiddleware()  Route.HttpFunc {
  return func(c *Route.Context) {
    var param interface{}
    if strings.ToLower(c.Request.Method) == "post"{
      param=c.Request.Form
    }else{
      param=c.Request.URL.Query()
    }
    body, _ := ioutil.ReadAll(c.Request.Body)
    c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(body))
    LogHelper.LoggerStd().Debug("request\r\nurl:%s\r\naddr: %s\r\nheader: %s\r\ncooke:%s\r\nmethod: %s\r\nparam:%s\r\nbody:%s",c.Request.URL, c.Request.RemoteAddr, c.Request.Header,c.Request.Cookies(), c.Request.Method, param, body)
		//Log.LogsFile("request.log", "request\r\nurl:%s\r\naddr: %s\r\nheader: %s\r\ncooke:%s\r\nmethod: %s\r\nparam:%s\r\nbody:%s",c.Request.URL, c.Request.RemoteAddr, c.Request.Header,c.Request.Cookies(), c.Request.Method, param, body)
    }
}
```
