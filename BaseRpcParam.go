package EricServer

/**默认传参**/
type RpcParam struct {
	JsonVer string `json:"json_ver"`
	RequestId string `json:"request_id"`
	Version string `json:"ver"`
}