package EricServer

import "embed"

/***/
type EricServerBuilder struct {
	serName         string //服务名称
	isDebug         bool
	isDefaultServer bool //是否是默认服务， 默认服务在主协程中开启

	host string //主域名
	port int    //端口

	tlsConf *HttpTlsConf //https配置

	httpRoute    []HttpRouteHandle        //注册的路由
	staticSource []*StaticStaticResources //注册静态资源访问

	templateDir    string //模板目录
	templateSubFix string //模板文件后缀

	uploadHttpDir   string //上传下载图片访问
	uploadSourceDir string //上传下载本地目录

	codeKey string //json code码字段
	msgKey  string //json 消息字段
	dataKey string //json 数据字段

	nodeId int        //节点ID (分布式)
	mode   MODE_CONST //启动模式 pro|dev 字符串

	publicAsset *embed.FS
}

func NewEricServerBuilder(publicAsset *embed.FS, serverName string, serverMode MODE_CONST, host string, port int) *EricServerBuilder {
	return &EricServerBuilder{
		serName: serverName,
		//isDebug:         debug,
		isDefaultServer: false,
		host:            host,
		port:            port,
		codeKey:         DEFAULT_CODE_KEY,
		msgKey:          DEFAULT_MSG_KEY,
		dataKey:         DEFAULT_DATA_KEY,
		nodeId:          1,
		mode:            serverMode,
		publicAsset:     publicAsset,
	}
}

func (this *EricServerBuilder) SetServerName(serverName string) {
	this.serName = serverName
}

//func (this *EricServerBuilder) SetDebug(debug bool) {
//	this.isDebug = debug
//}

func (this *EricServerBuilder) SetNodeId(nodeId int) {
	this.nodeId = nodeId
}

func (this *EricServerBuilder) SetTlsConf(tlsConf *HttpTlsConf) {
	this.tlsConf = tlsConf
}

/**
设置静态目录
@param httpDir string js css 等http访问
@param sourceDir string js css 绝对路径
*/
func (this *EricServerBuilder) SetStaticDir(httpDir, sourceDir string) *EricServerBuilder {
	if this.staticSource == nil {
		this.staticSource = []*StaticStaticResources{}
	}
	this.staticSource = append(this.staticSource, &StaticStaticResources{
		HttpDir:   httpDir,
		SourceDir: sourceDir,
	})
	return this
}

/**
批量设置静态资源目录
*/
func (this *EricServerBuilder) SetStaticDirs(staticSource ...*StaticStaticResources) *EricServerBuilder {
	if this.staticSource != nil && staticSource != nil {
		this.staticSource = staticSource
	}
	return this
}

/**
设置上传访问动态解析dir
@param httpDir string http访问目录
@param sourceDir string string本地存放目录
*/
func (this *EricServerBuilder) SetUploadDIr(httpDir, sourceDir string) *EricServerBuilder {
	this.uploadHttpDir = httpDir
	this.uploadSourceDir = sourceDir
	return this
}

/**
设置页面模板
@param sourceDir string 模板存放目录
@param subFix string 模板后缀
*/
func (this *EricServerBuilder) SetTemplate(sourceDir, subFix string) *EricServerBuilder {
	this.templateDir = sourceDir
	this.templateSubFix = subFix
	return this
}

/**
设置json响应key
@param string codeKey 响应code
@param string messageKey 响应message
@param string dataKey 响应data
*/
func (this *EricServerBuilder) SetResponseKey(codeKey, messageKey, dataKey string) *EricServerBuilder {
	this.codeKey = codeKey
	this.msgKey = messageKey
	this.dataKey = dataKey
	return this
}

/**
注册http路由
*/
func (this *EricServerBuilder) RegisterHttpRoute(httpRoute ...HttpRouteHandle) *EricServerBuilder {
	this.httpRoute = httpRoute
	return this
}
