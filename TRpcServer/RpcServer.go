package TRpcServer

import (
	"fmt"
	"net"
	"net/rpc/jsonrpc"
	"os"
)

type TRpcServer struct {
	netWork string
	add string
}

func NewRpcServer() *TRpcServer {
	return &TRpcServer{}
}

func (tRpc *TRpcServer)doConn(conn net.Conn)  {
	jsonrpc.ServeConn(conn)
}

func (tRpc *TRpcServer)Run(newWork, addr string)  {
	//err := Router.RpcApiRegister()
	var err error
	if err!=nil{
		//HelperLog.LogsOutF("api register error  %v", err)
	}

	listens, err := net.Listen(newWork, addr)
	if err!=nil{
		//HelperLog.LogsOutF("net.Listen tcp  %v", err)
	}
	_,_=fmt.Fprintf(os.Stdout, "rpc server start in %s \n", addr)
	defer  listens.Close()

	for {
		conn, err := listens.Accept()
		if err !=nil{
			//HelperLog.LogsOutF("listens.Accept: %v", err)
		}
		go tRpc.doConn(conn)
	}

}