package EricServer

import (
	"embed"
	"fmt"
	"gitee.com/tym_hmm/go-helper/LogHelper"
	Route "gitee.com/tym_hmm/go-web-router"
	"github.com/unrolled/secure"
	"log"
	"net/http"
	"strings"
)

type HttpTlsConf struct {
	certFilePath string
	keyFilePath  string
}

func NewHttpTlsConf(certFilePath string, keyFilePath string) *HttpTlsConf {
	return &HttpTlsConf{
		certFilePath: certFilePath,
		keyFilePath:  keyFilePath,
	}
}

type HttpRouteHandle func(e *Route.Engine)
type StaticStaticResources struct {
	HttpDir   string //http访问
	SourceDir string //本地路径
}

/**
http服务
*/
type httpServer struct {
	serName      string
	isDeBug      bool
	isTls        bool
	tlsConf      *HttpTlsConf
	routerEngine *Route.Engine
	publicAsset  *embed.FS
}

func NewHttpServer(publicAsset *embed.FS) *httpServer {
	return NewHttpServerMode(publicAsset, false)
}

/**
静态声明
*/
func NewHttpServerMode(publicAsset *embed.FS, isDebug bool) *httpServer {
	return &httpServer{
		isDeBug:      isDebug,
		routerEngine: Route.EngineNewMode(publicAsset, isDebug),
		publicAsset:  publicAsset,
	}
}

func (hs *httpServer) SetServerName(serName string) {
	hs.serName = serName
}

func (hs *httpServer) IsDebug(debug bool) *httpServer {
	hs.isDeBug = debug
	return hs
}

func (hs *httpServer) SetTls(tlsConf *HttpTlsConf) {
	if tlsConf != nil {
		hs.isTls = true
		hs.tlsConf = tlsConf
	} else {
		hs.isTls = false
		hs.tlsConf = nil
	}
}

/**
静态资源注册[js css等]
@param string httpDir http访问目录
@param string sourceDir 程序相对目录
*/
func (hs *httpServer) RegisterStaticResource(httpDir, sourceDir string) {
	if hs.publicAsset != nil && len(strings.TrimSpace(httpDir)) > 0 && len(strings.TrimSpace(sourceDir)) > 0 {
		hs.routerEngine.StaticFS(httpDir, hs.publicAsset, sourceDir)
	}
}

/**
多个静态资源注册[js css 等]
*/
func (hs *httpServer) RegisterStaticResources(statics ...*StaticStaticResources) {

	if statics != nil && len(statics) > 0 {

		for _, v := range statics {
			hs.RegisterStaticResource(v.HttpDir, v.SourceDir)
		}
	}
}

/**
本地附件仓库注册
@param string httpDir  http访问目录
@param string sourceDir 程序相对目录
*/
func (hs *httpServer) RegisterStoreSource(httpDir, sourceDir string) {
	if len(strings.TrimSpace(httpDir)) > 0 && len(strings.TrimSpace(sourceDir)) > 0 {
		hs.routerEngine.AttachFs(httpDir, sourceDir)
	}
}

/**
设置模板参数
@param string templateDir  模板存放目录
@param string templateSubFix  模板后缀
*/
func (hs *httpServer) RegisterTemplate(templateDir, templateSubFix string) {
	hs.routerEngine.SetTemplate(templateDir, templateSubFix)
}

/**
设置jsonKey
@param string codeKey code响应参数
@param string msgKey 消息响应参数
@param string dataKey data数据响应参数
*/
func (hs *httpServer) SetJsonKey(codeKey, msgKey, dataKey string) {
	hs.routerEngine.SetResponseKeyCode(codeKey)
	hs.routerEngine.SetResponseKeyMessage(msgKey)
	hs.routerEngine.SetResponseKeyData(dataKey)
}

/**
路由注册
*/
func (hs *httpServer) RegisterRoute(routeHandle ...HttpRouteHandle) {
	for _, v := range routeHandle {
		v(hs.routerEngine)
	}
}

func (hs *httpServer) Run(host string, port int) error {
	if hs.isTls && hs.tlsConf != nil {
		return hs.httpTLS(host, port, hs.tlsConf)
	} else {
		return hs.http(host, port)
	}
}

func (hs *httpServer) http(host string, port int) error {
	hs.routerEngine.CreateRouter()
	addr := fmt.Sprintf("%s:%d", host, port)
	LogHelper.LoggerStd().Info("http server [%s] Listener :%s, debug mode:%t", hs.serName, addr, hs.isDeBug)
	err := http.ListenAndServe(addr, hs.routerEngine)
	if err != nil {
		LogHelper.LoggerStd().Fatal("http server start error : %+v", err)
		return err
	}
	return nil
}

/**
https配置
@param host string 服务地址
@param port int 服务端口
@param certFilePath string 证书地址
@param keyFilePath string 证密钥文地址
*/
func (hs *httpServer) httpTLS(host string, port int, tslConf *HttpTlsConf) error {
	hs.routerEngine.CreateRouter()
	addr := fmt.Sprintf("%s:%d", host, port)
	LogHelper.LoggerStd().Info("https server [%s] Listener : %s,  debug mode:%t certFilePath:%s, keyFieldPath:%s", hs.serName, addr, hs.isDeBug, tslConf.certFilePath, tslConf.keyFilePath)

	secureMiddleware := secure.New(secure.Options{
		SSLRedirect: true,
		//SSLHost:     ":8443", // This is optional in production. The default behavior is to just redirect the request to the HTTPS protocol. Example: http://github.com/some_page would be redirected to https://github.com/some_page.
	})

	appHandle := secureMiddleware.Handler(hs.routerEngine)
	log.Fatal(http.ListenAndServeTLS(addr, tslConf.certFilePath, tslConf.keyFilePath, appHandle))
	return nil
}
