module gitee.com/tym_hmm/eric-http

go 1.16

require (
	gitee.com/tym_hmm/go-helper v1.1.49
	gitee.com/tym_hmm/go-web-router v1.1.9
	gitee.com/tym_hmm/hlog v1.0.6 // indirect
	github.com/unrolled/secure v1.13.0
)
