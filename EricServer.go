package EricServer

import (
	"embed"
	"os"
)

type MODE_CONST string

const (
	MODE_CONST_DEV MODE_CONST = "dev"
	MODE_CONST_PRO MODE_CONST = "pro"
)

const (
	DEFAULT_CODE_KEY = "code"
	DEFAULT_MSG_KEY  = "message"
	DEFAULT_DATA_KEY = "data"
)

type EricServer struct {
	defaultBuilder *EricServerBuilder
	builder        []*EricServerBuilder
}

func NewEricServer(publicAsset *embed.FS) *EricServer {
	var defaultBuilder = &EricServerBuilder{
		serName:     "default_server",
		codeKey:     DEFAULT_CODE_KEY,
		msgKey:      DEFAULT_MSG_KEY,
		dataKey:     DEFAULT_DATA_KEY,
		publicAsset: publicAsset,
	}
	return &EricServer{defaultBuilder: defaultBuilder}
}

/**
注册多个服务
*/
func (s *EricServer) RegisterServerBuilder(builder ...*EricServerBuilder) *EricServer {
	if builder != nil && len(builder) > 0 {
		s.builder = builder
	}
	return s
}

/**
设置静态目录
@param httpDir string js css 等http访问
@param sourceDir string js css 绝对路径
*/
func (s *EricServer) SetStaticDir(httpDir, sourceDir string) *EricServer {

	if s.defaultBuilder.staticSource == nil {
		s.defaultBuilder.staticSource = []*StaticStaticResources{}
	}
	s.defaultBuilder.staticSource = append(s.defaultBuilder.staticSource, &StaticStaticResources{
		HttpDir:   httpDir,
		SourceDir: sourceDir,
	})
	return s
}

//注册静态资源访问
func (s *EricServer) SetStaticDirs(staticSource ...*StaticStaticResources) *EricServer {
	if s.defaultBuilder.staticSource != nil && staticSource != nil {
		s.defaultBuilder.staticSource = staticSource
	}
	return s
}

/**
设置上传访问动态解析dir
*/
func (s *EricServer) SetUploadDir(httpDIr, sourceDIr string) *EricServer {
	s.defaultBuilder.uploadHttpDir = httpDIr
	s.defaultBuilder.uploadSourceDir = sourceDIr
	return s
}

/**
设置模板
@param sourceDir string 模板存放目录
@param subFix string 模板后缀
*/
func (s *EricServer) SetTemplate(sourceDir, subFix string) *EricServer {
	s.defaultBuilder.templateDir = sourceDir
	s.defaultBuilder.templateSubFix = subFix
	return s
}

//设置https 配置
func (s *EricServer) SetTlsConf(tlsConf *HttpTlsConf) {
	s.defaultBuilder.tlsConf = tlsConf
}

/**
设置json响应key
@param string codeKey 响应code
@param string messageKey 响应message
@param string dataKey 响应data
*/
func (s *EricServer) SetResponseKey(codeKey, messageKey, dataKey string) *EricServer {
	s.defaultBuilder.codeKey = codeKey
	s.defaultBuilder.msgKey = messageKey
	s.defaultBuilder.dataKey = dataKey
	return s
}

/**
注册http路由
*/
func (s *EricServer) RegisterHttpRoute(httpRoute ...HttpRouteHandle) *EricServer {
	s.defaultBuilder.httpRoute = httpRoute
	return s
}

func (s *EricServer) Start(host string, port int, nodeId int, mode MODE_CONST) {
	s.defaultBuilder.host = host
	s.defaultBuilder.port = port
	s.defaultBuilder.nodeId = nodeId
	s.defaultBuilder.mode = mode

	s.parseFlag()
	//s.confParse()
	//HelperLog.Log.Debug("config %+v", s.serverConfig)
	s.startHttpServer()

}

/**
解析命令行
*/
func (s *EricServer) parseFlag() {
	//if s.defaultBuilder.mode == MODE_CONST_DEV {
	//	s.defaultBuilder.isDebug = true
	//}
}

func (s *EricServer) startHttpServer() {
	if s.builder != nil && len(s.builder) > 0 {
		//var wg sync.WaitGroup
		for _, v := range s.builder {
			itemBuild := v
			if itemBuild != nil {
				//wg.Add(1)
				go func() {
					//defer wg.Done()
					err := s.startBuilder(itemBuild)
					if err != nil {
						os.Exit(-1)
					}
				}()
			}
		}
		//wg.Wait()
	}
	err := s.startBuilder(s.defaultBuilder)
	if err != nil {
		os.Exit(-1)
	}
}

func (this *EricServer) startBuilder(builder *EricServerBuilder) error {
	if builder.mode == MODE_CONST_DEV {
		builder.isDebug = true
	}
	httpServer := NewHttpServerMode(builder.publicAsset, builder.isDebug)
	httpServer.SetServerName(builder.serName)
	httpServer.SetTls(builder.tlsConf)
	//注册模板
	httpServer.RegisterTemplate(builder.templateDir, builder.templateSubFix)
	//注册静态资源访问
	//httpServer.RegisterStaticResource(s.staticHttpDir, s.staticSourceDir)
	httpServer.RegisterStaticResources(builder.staticSource...)
	//注册上传下载图片访问
	httpServer.RegisterStoreSource(builder.uploadHttpDir, builder.uploadSourceDir)
	//注册路由
	httpServer.RegisterRoute(builder.httpRoute...)
	//设置响应json key
	httpServer.SetJsonKey(builder.codeKey, builder.msgKey, builder.dataKey)
	err := httpServer.Run(builder.host, builder.port)
	return err
}
