package EricServer

import "fmt"

// 定义错误码
type Errno struct {
	err error
	Code    int
	Message string
}

func (err Errno) Error() string {
	return err.Message
}

// 定义错误
type Err struct {
	Code    int    // 错误码
	Message string // 展示给用户看的
	Errord  error  // 保存内部错误信息
}

func (err *Err) Error() string {
	return fmt.Sprintf("Err - code: %d, message: %s, error: %s", err.Code, err.Message, err.Errord)
}
