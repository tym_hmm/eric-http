package main

import (
	"bytes"
	"embed"
	Server "gitee.com/tym_hmm/eric-http"
	"gitee.com/tym_hmm/go-helper/LogHelper"
	Route "gitee.com/tym_hmm/go-web-router"
	"io/ioutil"
	"strings"
)

/**
静态打包处理需开启
**/
//go:embed Asset
var publicAsset embed.FS

func main() {

	server := Server.NewEricServer(&publicAsset)
	server.
		//设置模板
		SetTemplate("Asset/Template", ".html").
		//设置json响应
		SetResponseKey("code", "message", "data").
		//设置静态资源加载 js css ,会打包到二进制包中
		SetStaticDir("/static/", "Asset/Static").
		//设置资源上传及访问目录
		SetUploadDir("/upload", "/Store").
		//设置路由
		RegisterHttpRoute(ApiRouteNew, HttpRouteNew).
		RegisterServerBuilder(HttpSBuilder(&publicAsset)).
		Start("0.0.0.0", 8081, 1, Server.MODE_CONST_DEV)
}

func HttpSBuilder(publicAsset *embed.FS) *Server.EricServerBuilder {
	serverMode := Server.MODE_CONST_DEV
	httpsBuild := Server.NewEricServerBuilder(publicAsset, "https_server", serverMode, "0.0.0.0", 443)
	httpsBuild.RegisterHttpRoute(HttpsRouteNew)
	httpsBuild.SetTemplate("Asset/Template", ".html")
	httpsBuild.SetStaticDir("/static/", "Asset/Static")
	httpsBuild.SetUploadDIr("/static/", "Asset/Static")
	httpsBuild.SetTlsConf(Server.NewHttpTlsConf("./ssl/demo.pem", "./ssl/demo.key"))
	return httpsBuild
}

func HttpsRouteNew(e *Route.Engine) {
	e.Get("https", func(c *Route.Context) {
		c.Writer.Write([]byte("https"))
	})
}

/*
*
http 路由注册
*/
func HttpRouteNew(e *Route.Engine) {
	e.Get("/", RequestMiddleware(), func(c *Route.Context) {
		//多模板解析
		//c.TemplateMultiple()
		//单模板解析
		//c.Template()
		_, _ = c.Writer.Write([]byte("这是是ttp路由"))
	})
}

/*
*
api 路由注册
*/
func ApiRouteNew(e *Route.Engine) {

	v1 := e.Group("/v1")
	v1.UseMiddleware(RequestMiddleware())
	{
		v1.Get("/api", func(c *Route.Context) {
			c.JsonResponse(100, "成功", nil)
		})

		v1.Get("/:id", func(c *Route.Context) {
			id := c.GetParam("id")
			var data = make(map[string]interface{}, 1)
			data["id"] = id
			c.JsonResponse(100, "成功", data)
		})
	}
}

/*
*
请求日志拦截
*/
func RequestMiddleware() Route.HttpFunc {
	return func(c *Route.Context) {
		var param interface{}
		if strings.ToLower(c.Request.Method) == "post" {
			param = c.Request.Form
		} else {
			param = c.Request.URL.Query()
		}
		body, _ := ioutil.ReadAll(c.Request.Body)
		c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(body))
		LogHelper.LoggerStd().Debug("request\r\nurl:%s\r\naddr: %s\r\nheader: %s\r\ncooke:%s\r\nmethod: %s\r\nparam:%s\r\nbody:%s", c.Request.URL, c.Request.RemoteAddr, c.Request.Header, c.Request.Cookies(), c.Request.Method, param, body)
		//Log.LogsFile("request.log", "request\r\nurl:%s\r\naddr: %s\r\nheader: %s\r\ncooke:%s\r\nmethod: %s\r\nparam:%s\r\nbody:%s",c.Request.URL, c.Request.RemoteAddr, c.Request.Header,c.Request.Cookies(), c.Request.Method, param, body)
	}
}
