package EricServer

import "fmt"

type KError struct {
	Code int
	Message string
	err error
}

func NewKError(code int, message string, err ...error) *KError {
	errs:=[]error{}
	errs=append(errs, err...)
	return &KError{
		Code:    code,
		Message: message,
		err:errs[0],
	}
}

func (t *KError) Error() string {
	errs:=""
	if t.err !=nil{
		errs=t.err.Error()
	}
	return fmt.Sprintf("Exception (%d) Reason: %q, %s", t.Code, t.Message, "err:"+errs)
}


